//Find the average salary of based on country. ( Groupd it based on country and then find the average ).

function getAverageSalaryByCountry(personData){
    if(Array.isArray(personData)){
        let sumSalariesByCountry = {};
        let countByCountries = {};
        for (let person of personData) {
            if (!sumSalariesByCountry[person.location]) {
                sumSalariesByCountry[person.location] = 0;
                countByCountries[person.location] = 0;
            }
            sumSalariesByCountry[person.location] += person.salaryAfterModification; 
            countByCountries[person.location] += 1;
        }
        let getAverageSalaryByCountries = {};
        for(let country in sumSalariesByCountry){
            getAverageSalaryByCountries[country] = sumSalariesByCountry[country] / countByCountries[country];
        }
        return getAverageSalaryByCountries;
    }
    else{
        return {};
    }
}

module.exports = getAverageSalaryByCountry;
