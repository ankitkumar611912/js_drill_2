//4. Find the sum of all salaries.

function totalSalary(personData){
    if(Array.isArray(personData)){
        let totalSalary = 0;
        for(let val of personData){
            totalSalary += val.salaryAfterModification;
        }
        return totalSalary;
    }else{
        return 0;
    }
}

module.exports = totalSalary;