//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )


function getAllWebDevelopers(personData){
    if(Array.isArray(personData)){
        let allWebDevelopers = [];{
            for(let val of personData){
                if(val.job.includes("Web Developer")){
                    allWebDevelopers.push(val);
                }
            }
        }
        return allWebDevelopers;
    }
    else{
        return [];
    }
}

module.exports = getAllWebDevelopers;