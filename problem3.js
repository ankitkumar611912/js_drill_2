// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function getSalaryInFactor1000(salaryInNumber){
    if(Array.isArray(salaryInNumber)){
        for(let val of salaryInNumber){
            let updatedSalary = val.salary * 1000;
            val.salaryAfterModification = updatedSalary;
        }
        return salaryInNumber;
    }
    else{
        return [];
    }
}


module.exports = getSalaryInFactor1000;