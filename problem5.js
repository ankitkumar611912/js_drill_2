//5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).


function getSalaryByCountry(personData){
    if(Array.isArray(personData)){
        let sumSalariesByCountry = {};
        for (let person of personData) {
            if (!sumSalariesByCountry[person.location]) {
                sumSalariesByCountry[person.location] = 0;
            }
            sumSalariesByCountry[person.location] += person.salaryAfterModification; 
        }
        return sumSalariesByCountry;
    }
    else{
        return {};
    }
}
module.exports = getSalaryByCountry;
