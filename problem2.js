//2. Convert all the salary values into proper numbers instead of strings.

function toConvertStringSalaryToNumber(personData){
    if(Array.isArray(personData)){
        for(let val of personData){
            let salaryString = val.salary;
            let salaryInNumber = parseFloat(salaryString.replace("$",""));
            val.salary = salaryInNumber;
        }
        return personData;
    }
    else{
        return []
    }
}
module.exports = toConvertStringSalaryToNumber;